﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace ServisUygulama.Models
{
    [Table("SchoolBu")]
    public class SchoolBus
    {
        [Key]
        public int BusID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.HomeTexts),ErrorMessageResourceName = "Plakaerrormesage")]
        [Display(Name = "Plate", ResourceType = typeof(Resources.HomeTexts))]
        public string Plate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.HomeTexts), ErrorMessageResourceName = "Modelerrormesage")]
        [Display(Name = "Model", ResourceType = typeof(Resources.HomeTexts))]
        public int Model { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.HomeTexts), ErrorMessageResourceName = "Numbererrormesage")]
        [Display(Name = "Numberofseats", ResourceType = typeof(Resources.HomeTexts))]
        public int KoltukSayısı { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.HomeTexts), ErrorMessageResourceName = "Drivererrormesage")]
        [Display(Name = "Şöför")]
        public int DriverID { get; set; }
        [ForeignKey("DriverID")]
        public Driver Driver { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.HomeTexts), ErrorMessageResourceName = "Branderrormesage")]
        [Display(Name = "Marka")]
        public int MarkaID { get; set; }
        [ForeignKey("MarkaID")]
        public Brand Brand { get; set; }

        public virtual ICollection<ApplicationUser> User { get; set; }

        public SchoolBus()
        {
            User = new List<ApplicationUser>();
        }




    }
}