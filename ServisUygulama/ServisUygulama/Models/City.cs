﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServisUygulama.Models
{
    [Table("Cities")]
    public class City
    {


        [Key]
        public int CityID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public virtual List<ApplicationUser> Users { set; get; }
        public virtual List<Driver> Drivers { set; get; }
    }
}