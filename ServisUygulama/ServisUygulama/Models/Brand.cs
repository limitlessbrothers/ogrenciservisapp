﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServisUygulama.Models
{
    [Table("Brands")]
    public class Brand
    {
        [Key]
        public int BrandID { get; set; }

        [Required,MaxLength(100)]
        [Display(Name = "Marka :")]
        public string BrandName { get; set; }

        public virtual List<SchoolBus> SchoolBu { set; get; }

    }
}