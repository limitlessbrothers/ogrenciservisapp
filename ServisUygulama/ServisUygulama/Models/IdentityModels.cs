﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace ServisUygulama.Models
{
    // ApplicationUser sınıfınıza daha fazla özellik ekleyerek kullanıcıya profil verileri ekleyebilirsiniz. Daha fazla bilgi için lütfen https://go.microsoft.com/fwlink/?LinkID=317594 adresini ziyaret edin.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }

      

        public int UniversityID { get; set; }
        [ForeignKey("UniversityID")]
        public University University { get; set; }

        public int CityID { get; set; }
        [ForeignKey("CityID")]
        public City City { get; set; }

        public virtual List<Announcement> Announcements { set; get; }

        public virtual ICollection<SchoolBus> SchoolBus { get; set; }

        public ApplicationUser()
        {
            SchoolBus = new List<SchoolBus>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // authenticationType özelliğinin CookieAuthenticationOptions.AuthenticationType içinde tanımlanmış olanla eşleşmesi gerektiğini unutmayın
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Özel kullanıcı taleplerini buraya ekle
            return userIdentity;
        }

    }
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base(){ }

        public ApplicationRole(string roleName) : base(roleName) { }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("Users").Property(p=>p.Id).HasColumnName("UserID");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("userClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("userLogins");
            modelBuilder.Entity<IdentityUserRole>().ToTable("userRoles");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");

        }

        public System.Data.Entity.DbSet<ServisUygulama.Models.RoleViewModel> RoleViewModels { get; set; }
        public DbSet<University> Universities { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Announcement> Announcements { get; set; }

        public System.Data.Entity.DbSet<ServisUygulama.Models.Brand> Brands { get; set; }

        public System.Data.Entity.DbSet<ServisUygulama.Models.Driver> Drivers { get; set; }

        public System.Data.Entity.DbSet<ServisUygulama.Models.SchoolBus> SchoolBus { get; set; }
    }
}