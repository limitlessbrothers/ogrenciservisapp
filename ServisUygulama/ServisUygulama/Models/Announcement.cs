﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServisUygulama.Models
{
    [Table("Announcements")]
    public class Announcement
    {
        [Key]
        public int AnnouncementID { get; set; }

        [Required,MaxLength(100)]
        public string Title { get; set; }

        [Required,MaxLength(4000)]
        public string Content { get; set; }

        [DisplayName("Eklenme Tarihi")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AddTime { get; set; }

        public string UserID { get; set; }
        [ForeignKey("UserID")]
        public ApplicationUser Users { get; set; }



    }
}