﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServisUygulama.Models
{
    [Table("Universities")]
    public class University
    {
        
        
            [Key]
            public int UniversiyID { get; set; }

            [Required,MaxLength(100)]
            public string Name { get; set; }

            public virtual List<ApplicationUser> Users { set; get; }
      

    }
}