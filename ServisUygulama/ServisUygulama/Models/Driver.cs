﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServisUygulama.Models
{
    [Table("Drivers")]
    public class Driver
    {
        [Key]
        public int DriverID { get; set; }

        [Required(ErrorMessage ="Şöför adı ve soyadı zorunludur."),MaxLength(100)]
        public string DriverName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage ="Telefon numarası zorunludur.")]
        public string Phone { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [Display(Name = "Doğum Tarihiniz")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
        
        [Required(ErrorMessage = "Şehir zorunludur.")]
        public int CityID { get; set; }
        [ForeignKey("CityID")]
        public City City { get; set; }

        public virtual List<SchoolBus> SchoolBu { set; get; }

    }
}