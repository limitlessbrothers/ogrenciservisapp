﻿using System.Web;
using System.Web.Optimization;

namespace ServisUygulama
{
    public class BundleConfig
    {
        // Paketleme hakkında daha fazla bilgi için lütfen https://go.microsoft.com/fwlink/?LinkId=301862 adresini ziyaret edin
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/vendor/jquery/jquery.min.js",
                        "~/vendor/bootstrap/js/bootstrap.min.js",
                        "~/vendor/metisMenu/metisMenu.min.js",
                        "~/vendor/raphael/raphael.min.js",
                        "~/vendor/morrisjs/morris.min.js",
                        "~/Scripts/morris-data.js",
                        "~/Scripts/sb-admin-2.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Geliştirme yapmak ve öğrenmek için Modernizr'ın geliştirme sürümünü kullanın. Daha sonra,
            // üretim için hazır. https://modernizr.com adresinde derleme aracını kullanarak yalnızca ihtiyacınız olan testleri seçin.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/vendor/bootstrap/css/bootstrap.css",
                      "~/Content/sb-admin-2.css",
                      "~/vendor/bootstrap-social/bootstrap-social.css",
                      "~/vendor/datatables/css/dataTables.bootstrap.css",
                      "~/vendor/metisMenu/metisMenu.min.css",
                      "~/vendor/morrisjs/morris.css",
                       "~/vendor/font-awesome/css/font-awesome.min.css"));
        }
    }
}
