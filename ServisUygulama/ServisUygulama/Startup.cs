﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ServisUygulama.Startup))]
namespace ServisUygulama
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
