namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusCreate1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SchoolBusApplicationUsers",
                c => new
                {
                    SchoolBus_BusID = c.Int(nullable: false),
                    ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.SchoolBus_BusID, t.ApplicationUser_Id })
                .ForeignKey("dbo.SchoolBu", t => t.SchoolBus_BusID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.ApplicationUser_Id, cascadeDelete: true)
                .Index(t => t.SchoolBus_BusID)
                .Index(t => t.ApplicationUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchoolBusApplicationUsers", "ApplicationUser_Id", "dbo.Users");
            DropForeignKey("dbo.SchoolBusApplicationUsers", "SchoolBus_BusID", "dbo.SchoolBu");
            DropIndex("dbo.SchoolBusApplicationUsers", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.SchoolBusApplicationUsers", new[] { "SchoolBus_BusID" });
            DropTable("dbo.SchoolBusApplicationUsers");
        }
    }
}
