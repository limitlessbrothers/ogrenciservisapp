namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAnnouncement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Announcements",
                c => new
                    {
                        AnnouncementID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Content = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.AnnouncementID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Announcements");
        }
    }
}
