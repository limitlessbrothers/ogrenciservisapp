namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CityForeingKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "City_CityID", "dbo.Cities");
            DropIndex("dbo.Users", new[] { "City_CityID" });
            RenameColumn(table: "dbo.Users", name: "City_CityID", newName: "CityID");
            AlterColumn("dbo.Users", "CityID", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "CityID");
            AddForeignKey("dbo.Users", "CityID", "dbo.Cities", "CityID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "CityID", "dbo.Cities");
            DropIndex("dbo.Users", new[] { "CityID" });
            AlterColumn("dbo.Users", "CityID", c => c.Int());
            RenameColumn(table: "dbo.Users", name: "CityID", newName: "City_CityID");
            CreateIndex("dbo.Users", "City_CityID");
            AddForeignKey("dbo.Users", "City_CityID", "dbo.Cities", "CityID");
        }
    }
}
