namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CityDelete : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "City");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "City", c => c.String());
        }
    }
}
