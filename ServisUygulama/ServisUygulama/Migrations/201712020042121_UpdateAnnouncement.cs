namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAnnouncement : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Announcements", "AddTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Announcements", "AddTime");
        }
    }
}
