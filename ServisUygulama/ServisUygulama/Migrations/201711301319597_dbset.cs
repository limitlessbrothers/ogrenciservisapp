namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbset : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "University_UniversiyID", "dbo.Universities");
            DropIndex("dbo.Users", new[] { "University_UniversiyID" });
            RenameColumn(table: "dbo.Users", name: "University_UniversiyID", newName: "UniversityID");
            AlterColumn("dbo.Users", "UniversityID", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "UniversityID");
            AddForeignKey("dbo.Users", "UniversityID", "dbo.Universities", "UniversiyID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UniversityID", "dbo.Universities");
            DropIndex("dbo.Users", new[] { "UniversityID" });
            AlterColumn("dbo.Users", "UniversityID", c => c.Int());
            RenameColumn(table: "dbo.Users", name: "UniversityID", newName: "University_UniversiyID");
            CreateIndex("dbo.Users", "University_UniversiyID");
            AddForeignKey("dbo.Users", "University_UniversiyID", "dbo.Universities", "UniversiyID");
        }
    }
}
