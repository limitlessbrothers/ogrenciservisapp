namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateCİty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        CityID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.CityID);
            
            AddColumn("dbo.Users", "City_CityID", c => c.Int());
            CreateIndex("dbo.Users", "City_CityID");
            AddForeignKey("dbo.Users", "City_CityID", "dbo.Cities", "CityID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "City_CityID", "dbo.Cities");
            DropIndex("dbo.Users", new[] { "City_CityID" });
            DropColumn("dbo.Users", "City_CityID");
            DropTable("dbo.Cities");
        }
    }
}
