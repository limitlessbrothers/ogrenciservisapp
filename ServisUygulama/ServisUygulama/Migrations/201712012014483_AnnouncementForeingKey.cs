namespace ServisUygulama.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnnouncementForeingKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Announcements", "UserID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Announcements", "UserID");
            AddForeignKey("dbo.Announcements", "UserID", "dbo.Users", "UserID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Announcements", "UserID", "dbo.Users");
            DropIndex("dbo.Announcements", new[] { "UserID" });
            DropColumn("dbo.Announcements", "UserID");
        }
    }
}
