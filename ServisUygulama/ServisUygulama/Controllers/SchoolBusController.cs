﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ServisUygulama.Models;

namespace ServisUygulama.Controllers
{
    public class SchoolBusController : Controller
    {
        public ApplicationDbContext db = new ApplicationDbContext();

        // GET: SchoolBus
        [Authorize]
        public async Task<ActionResult> Index()
        {
            var schoolBus = db.SchoolBus.Include(s => s.Brand).Include(s => s.Driver);
            return View(await schoolBus.ToListAsync());
        }

        // GET: SchoolBus/Details/5
        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SchoolBus schoolBus = await db.SchoolBus.FindAsync(id);
            db.SchoolBus.Include(x => x.Brand).Include(x => x.Driver).ToList();
            
            if (schoolBus == null)
            {
                return HttpNotFound();
            }
            return View(schoolBus);
        }

        // GET: SchoolBus/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.MarkaID = new SelectList(db.Brands, "BrandID", "BrandName");
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverName");
            return View();
        }

        // POST: SchoolBus/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BusID,Plate,Model,KoltukSayısı,DriverID,MarkaID")] SchoolBus schoolBus)
        {
            if (ModelState.IsValid)
            {
                db.SchoolBus.Add(schoolBus);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MarkaID = new SelectList(db.Brands, "BrandID", "BrandName", schoolBus.MarkaID);
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverName", schoolBus.DriverID);
            return View(schoolBus);
        }

        // GET: SchoolBus/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SchoolBus schoolBus = await db.SchoolBus.FindAsync(id);
            if (schoolBus == null)
            {
                return HttpNotFound();
            }
            ViewBag.MarkaID = new SelectList(db.Brands, "BrandID", "BrandName", schoolBus.MarkaID);
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverName", schoolBus.DriverID);
            return View(schoolBus);
        }

        // POST: SchoolBus/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit([Bind(Include = "BusID,Plate,Model,KoltukSayısı,DriverID,MarkaID")] SchoolBus schoolBus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(schoolBus).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.MarkaID = new SelectList(db.Brands, "BrandID", "BrandName", schoolBus.MarkaID);
            ViewBag.DriverID = new SelectList(db.Drivers, "DriverID", "DriverName", schoolBus.DriverID);
            return View(schoolBus);
        }

        // GET: SchoolBus/KayitOl/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> KayitOl(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
            
        }


        // GET: SchoolBus/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SchoolBus schoolBus = await db.SchoolBus.FindAsync(id);
            if (schoolBus == null)
            {
                return HttpNotFound();
            }
            return View(schoolBus);
        }

        // POST: SchoolBus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SchoolBus schoolBus = await db.SchoolBus.FindAsync(id);
            db.SchoolBus.Remove(schoolBus);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
