﻿using Microsoft.AspNet.Identity;
using ServisUygulama.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ServisUygulama.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext context = new ApplicationDbContext();


        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Kullanici = User.Identity.GetUserName();

            var role = (from r in context.Roles where r.Name.Contains("Admin") select r).FirstOrDefault();
            var users = context.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            var userVM = users.Select(user => new UserViewModel
            {
                Username = user.UserName,
                Email = user.Email,
                RoleName = "Admin"
            }).ToList();
            var model = new GroupedUserViewModel { Users = userVM};

           
            ViewBag.Okllist=context.Universities.ToList().Count();
            ViewBag.Servislist = context.SchoolBus.ToList().Count();
            ViewBag.Rolelist = context.Roles.ToList().Count();
            return View(context.Announcements.OrderByDescending(x=>x.AddTime).ToList());
        }
        [Authorize(Roles = "Admin")]
        public ActionResult OgrenciList()
        {
            var role = (from r in context.Roles where r.Name.Contains("Öğrenci") select r).FirstOrDefault();
            var users = context.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            var userVM = users.Select(user => new UserViewModel
            {
                UserID = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email,
                RoleName = "Öğrenci",
                UniversityName = (from i in context.Universities where i.UniversiyID == user.UniversityID select i).SingleOrDefault().Name
            }).ToList();
            
            var model = new GroupedUserViewModel { Users = userVM };
            return View(model);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Change(string LanguageAbbrevation, string returnUrl)
        {
        
            if (LanguageAbbrevation != null)
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageAbbrevation);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageAbbrevation);
                }

                HttpCookie cookie = new HttpCookie("Language");
                cookie.Value = LanguageAbbrevation;
                Response.Cookies.Add(cookie);
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
               
                return RedirectToAction("Index", "Home");


        }


       
    }
}